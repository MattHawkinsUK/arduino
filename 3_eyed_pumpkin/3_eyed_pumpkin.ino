//
// 3 Eyed Pumpkin
//
// This sketch randomly positions 3 servos
// for my 3-eyed pumpkin project.
//
// Three servos are connected to :
//   Pin 11 (D8)
//   Pin 12 (D9)
//   Pin 13 (D10)
//
// https://www.tech-spy.co.uk/
//
// Author : Matt Hawkins
// Date   : 31/10/2018
//

#include <Servo.h>

// Define constants
#define MINPOS   25
#define MAXPOS   138
#define MINDELAY 200
#define MAXDELAY 600

// Servo pins
#define SERVOPIN1 8
#define SERVOPIN1 9
#define SERVOPIN1 10

// Create servo objects
Servo myservo1;
Servo myservo2;
Servo myservo3;

// Find centre point based on servo min/max
int CTRPOS = (MINPOS + MAXPOS) / 2;

void setup() {
  myservo1.attach(8);  // attach servo on D8
  myservo2.attach(9);  // attach servo on D9
  myservo3.attach(10); // attach servo on D10
}

void loop() {
  // Randomly position servos between min and max
  moveServo(myservo1);
  moveServo(myservo2);
  moveServo(myservo3);
  delay(random(MINDELAY, MAXDELAY));
}

void moveServo(Servo servo) {
  // Random number:
  // 0   : centre position
  // 1-2 : random position between min and max
  // 3   : min position
  // 4   : max position
  switch (random(5)) {
    case 0:
      servo.write(CTRPOS);
      break;
    case 3:
      servo.write(MINPOS);
      break;
    case 4:
      servo.write(MAXPOS);
      break;
    default:
      servo.write(random(MINPOS, MAXPOS));
      break;
  }

}
