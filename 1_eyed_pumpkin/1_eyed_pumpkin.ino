//
// 1 Eyed Pumpkin
//
// This sketch randomly positions a servo
// for my 1-eyed pumpkin project.
//
// One servo is connected to :
//   Pin 11 (D8)
//
// https://www.tech-spy.co.uk/
//
// Author : Matt Hawkins
// Date   : 31/10/2018
//

#include <Servo.h>

// Define constants
#define MINPOS   25
#define MAXPOS   138
#define MINDELAY 100
#define MAXDELAY 500

// Servo pin
#define SERVOPIN 8

// Create servo objects
Servo myservo;

// Find centre point based on servo min/max
int CTRPOS = (MINPOS + MAXPOS) / 2;

void setup() {
  myservo.attach(SERVOPIN);  // attach servo on D8
}

void loop() {
  // Randomly position servo between min and max
  moveServo(myservo);
  delay(random(MINDELAY, MAXDELAY));
}

void moveServo(Servo servo) {
  // Random number:
  // 0   : centre position
  // 1-2 : random position between min and max
  // 3   : min position
  // 4   : max position
  switch (random(5)) {
    case 0:
      servo.write(CTRPOS);
      break;
    case 3:
      servo.write(MINPOS);
      break;
    case 4:
      servo.write(MAXPOS);
      break;
    default:
      servo.write(random(MINPOS, MAXPOS));
      break;
  }

}
