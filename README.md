# Arduino

A collection of Arduino Sketches. Mainly for the Arduino Nano.

* 1_eyed_pumpkin
A script to randomly move a single servo which is attached to a ping pong ball "eye".

* 3_eyed_pumpkin
A script to randomly move three servos which are attached to ping pong ball "eyes". Could be modified to control more servos if required.

# Hardware
These scripts were created to be run on the Arduino Nano but should work on other Arduino models. When using Servos I've tended to provide external power rather than relying on the USB port on my laptop.
